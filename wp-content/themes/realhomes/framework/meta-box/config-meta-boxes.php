<?php
/**
 * Register meta boxes
 */

add_filter( 'rwmb_meta_boxes', 'inspiry_register_meta_boxes' );

if( !function_exists( 'inspiry_register_meta_boxes' ) ) {
    function inspiry_register_meta_boxes() {

        // Make sure there's no errors when the plugin is deactivated or during upgrade
        if (!class_exists('RW_Meta_Box')) {
            return;
        }

        $meta_boxes = array();
        $prefix = 'REAL_HOMES_';

        // Video embed code meta box for video post format
        $meta_boxes[] = array(
            'id' => 'video-meta-box',
            'title' => __('קוד וידאו', 'framework'),
            'pages' => array('post'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('קוד וידאו', 'framework'),
                    'desc' => __('If you are not using self hosted videos then please provide the video embed code and remove the width and height attributes.', 'framework'),
                    'id' => "{$prefix}embed_code",
                    'type' => 'textarea',
                    'cols' => '20',
                    'rows' => '3'
                )
            )
        );


        // Gallery Meta Box
        $meta_boxes[] = array(
            'id' => 'gallery-meta-box',
            'title' => __('תמונות גלריה', 'framework'),
            'pages' => array('post'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('העלה תמונות לגלריה', 'framework'),
                    'id' => "{$prefix}gallery",
                    'desc' => __('Images should have minimum width of 830px and minimum height of 323px, Bigger size images will be cropped automatically.', 'framework'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 48
                )
            )
        );


        // Agents
        $agents_array = array(-1 => __('שם', 'framework'));
        $agents_posts = get_posts(array('post_type' => 'agent', 'posts_per_page' => -1, 'suppress_filters' => 0));
        if (!empty($agents_posts)) {
            foreach ($agents_posts as $agent_post) {
                $agents_array[$agent_post->ID] = $agent_post->post_title;
            }
        }

        // Property Details Meta Box
        $meta_boxes[] = array(
            'id' => 'property-meta-box',
            'title' => __('פרויקט', 'framework'),
            'pages' => array('property'),
            'tabs' => array(
                'details' => array(
                    'label' => __('פרטים בסיסיים', 'framework'),
                    'icon' => 'dashicons-admin-home',
                ),
                'gallery' => array(
                    'label' => __('גלרית תמונות', 'framework'),
                    'icon' => 'dashicons-format-gallery',
                ),
                'video' => array(
                    'label' => __('וידאו פרויקט', 'framework'),
                    'icon' => 'dashicons-format-video',
                ),
                'agent' => array(
                    'label' => __('סוכן', 'framework'),
                    'icon' => 'dashicons-businessman',
                ),
                'misc' => array(
                    'label' => __('שונות', 'framework'),
                    'icon' => 'dashicons-lightbulb',
                ),
                'home-slider' => array(
                    'label' => __('סליידר דף בית', 'framework'),
                    'icon' => 'dashicons-images-alt',
                ),
                'banner' => array(
                    'label' => __('Top Banner', 'framework'),
                    'icon' => 'dashicons-format-image',
                ),
            ),
            'tab_style' => 'left',
            'fields' => array(

                // Details
                array(
                    'id' => "{$prefix}property_price",
                    'name' => __('מחיר השכרה או מכירה( ספרות בלבד )', 'framework'),
                    'desc' => __('ערך לדוגמא:456256', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_price_postfix",
                    'name' => __('תעריף מחיר', 'framework'),
                    'desc' => __('ערך לדוגמא:לפי חודש', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_size",
                    'name' => __('גודל איזור ( ספרות בלבד )', 'framework'),
                    'desc' => __('ערך לדוגמא:2500', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_size_postfix",
                    'name' => __('אמת מידה', 'framework'),
                    'desc' => __('ערך לדוגמא:מ"ר', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_bedrooms",
                    'name' => __('מתחמי קניות', 'framework'),
                    'desc' => __('ערך לדוגמא: 4', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_bathrooms",
                    'name' => __('חדרי רחצה', 'framework'),
                    'desc' => __('ערך לדוגמא: 2', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_garage",
                    'name' => __('בתי ספר', 'framework'),
                    'desc' => __('ערך לדוגמא: 1', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_id",
                    'name' => __('מספר פרוייקט', 'framework'),
                    'desc' => __('יעזור לחפש פרויקטבמדויק.', 'framework'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'details',
                ),


                // Map
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'google_map_divider', // Not used, but needed
                    'tab' => 'details',
                ),
                array(
                    'name' => __('מפת פרויקט?', 'framework'),
                    'id' => "{$prefix}property_map",
                    'type' => 'radio',
                    'std' => 0,
                    'options' => array(
                        0 => __('הצג ', 'framework'),
                        1 => __('הסתר', 'framework')
                    ),
                    'columns' => 12,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_address",
                    'name' => __('כתובת פרויקט', 'framework'),
                    'desc' => __('השארת שדה ריק יגרופ להסתדרת המפה בפרטי הפרויקט.', 'framework'),
                    'type' => 'text',
                    'std' => get_option( 'theme_submit_default_address' ),
                    'columns' => 12,
                    'tab' => 'details',
                ),
                array(
                    'id' => "{$prefix}property_location",
                    'name' => __('מיקום פרויקטבגוגל מפות*', 'framework'),
                    'desc' => __('גרור את הסמןלמיקום הפרויקט שלך. תוכל גם להשתמש בשורת החיפוש כדי למצוא את הפרויקט..', 'framework'),
                    'type' => 'map',
                    'std' => get_option( 'theme_submit_default_location' ),   // 'latitude,longitude[,zoom]' (zoom is optional)
                    'style' => 'width: 95%; height: 400px',
                    'address_field' => "{$prefix}property_address",
                    'columns' => 12,
                    'tab' => 'details',
                ),

                // Gallery
                array(
                    'name' => __('סוג גלריה לשימוש', 'framework'),
                    'id' => "{$prefix}gallery_slider_type",
                    'type' => 'radio',
                    'std' => 'thumb-on-right',
                    'options' => array(
                        'thumb-on-right' => __('גלריה עם תבנית תמונה בימין', 'framework'),
                        'thumb-on-bottom' => __('גלריה עם תבנית תמונה בתחתית', 'framework')
                    ),
                    'columns' => 12,
                    'tab' => 'gallery',
                ),
                array(
                    'name' => __('גלרית תמונות פרויקט', 'framework'),
                    'id' => "{$prefix}property_images",
                    'desc' => __('לתמונות יהיה גודל מינימלי של 770פיקסלים על 386פיקסלים לימין, וגודל של 830 על 460 לתחתית. תמונות גדולות יותר יחתכו אוטומטית.', 'framework'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 48,
                    'columns' => 12,
                    'tab' => 'gallery',
                ),


                // Property Video
                array(
                    'id' => "{$prefix}tour_video_url",
                    'name' => __('סיור וירטואלי', 'framework'),
                    'desc' => __('סיור וירטואלי תומך ב. YouTube, Vimeo, SWF קובץ ו MOV File נתמכים', 'framework'),
                    'type' => 'text',
                    'columns' => 12,
                    'tab' => 'video',
                ),
                array(
                    'name' => __('Virtual Tour Video Image', 'framework'),
                    'id' => "{$prefix}tour_video_image",
                    'desc' => __('Provide an image that will be displayed as a place holder and when user will click over it the video will be opened in a lightbox. You must provide this image otherwise the video will not be displayed. Image should have minimum width of 818px and minimum height 417px. Bigger size images will be cropped automatically.', 'framework'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'columns' => 12,
                    'tab' => 'video',
                ),

                // Agents
                array(
                    'name' => __('מה להציג באופצית מידע על הסוכן ?', 'framework'),
                    'id' => "{$prefix}agent_display_option",
                    'type' => 'radio',
                    'std' => 'none',
                    'options' => array(
                        'my_profile_info' => __('מידע על הכותב.', 'framework'),
                        'agent_info' => __('מידע על הסוכן. ( בחר סוכן )', 'framework'),
                        'none' => __('ריק. (הסתר מידע)', 'framework'),
                    ),
                    'columns' => 12,
                    'tab' => 'agent',
                ),
                array(
                    'name' => __('סוכן', 'framework'),
                    'id' => "{$prefix}agents",
                    'type' => 'select',
                    'options' => $agents_array,
                    'columns' => 12,
                    'tab' => 'agent',
                ),

                // Misc
                array(
                    'name' => __('סמן את הפרויקט כשייך ?', 'framework'),
                    'id' => "{$prefix}featured",
                    'type' => 'radio',
                    'std' => 0,
                    'options' => array(
                        1 => __('כן ', 'framework'),
                        0 => __('לא', 'framework')
                    ),
                    'columns' => 12,
                    'tab' => 'misc',
                ),
                array(
                    'id' => "{$prefix}attachments",
                    'name' => __('נוספים', 'framework'),
                    'desc' => __('הוספת קובץ , Map images OR other documents to provide further details related to property.', 'framework'),
                    'desc' => __('הוספת קובץ PDF, תמונות מפה או מידע אחר על הפרויקט.', 'framework'),
                    'type' => 'file_advanced',
                    'mime_type' => '',
                    'columns' => 12,
                    'tab' => 'misc',
                ),
                array(
                    'id' => "{$prefix}property_private_note",
                    'name' => __('הערה פרטית', 'framework'),
                    'desc' => __('בתיבה זו תוכל לכתוב מידע פרטי על הפרויקט. שדה זה לא יוצג במקום אחר.', 'framework'),
                    'type' => 'textarea',
                    'std' => "",
                    'columns' => 12,
                    'tab' => 'misc',
                ),

                // Homepage Slider
                array(
                    'name' => __('תרצה להוסיף את הפרויקטלסליידר בדף הבית ?', 'framework'),
                    'desc' => __('אם כן, תצטרך להוסיף תמונת סליידר למטה.', 'framework'),
                    'id' => "{$prefix}add_in_slider",
                    'type' => 'radio',
                    'std' => 'no',
                    'options' => array(
                        'yes' => __('כן ', 'framework'),
                        'no' => __('לא', 'framework')
                    ),
                    'columns' => 12,
                    'tab' => 'home-slider',
                ),
                array(
                    'name' => __('תמונת סליידר', 'framework'),
                    'id' => "{$prefix}slider_image",
                    'desc' => __('The recommended image size is 2000px by 700px. You can use bigger or smaller image but try to keep the same height to width ratio and use the exactly same size images for all properties that will be added in slider.', 'framework'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'columns' => 12,
                    'tab' => 'home-slider',
                ),

                // Top Banner
                array(
                    'name' => __('תמונת באנר עליוןe', 'framework'),
                    'id' => "{$prefix}page_banner_image",
                    'desc' => __('Upload the banner image, If you want to change it for this property. Otherwise default banner image uploaded from theme options will be displayed. Image should have minimum width of 2000px and minimum height of 230px.', 'framework'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'columns' => 12,
                    'tab' => 'banner',
                )

            )
        );


        // Partners Meta Box
        $meta_boxes[] = array(
            'id' => 'partners-meta-box',
            'title' => __('מידע שותף', 'framework'),
            'pages' => array('partners'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('אתר URL', 'framework'),
                    'id' => "{$prefix}partner_url",
                    'desc' => __('מספק כתובת URL', 'framework'),
                    'type' => 'text',
                )
            )
        );


        // Agent Meta Box
        $meta_boxes[] = array(
            'id' => 'agent-meta-box',
            'title' => __('ספק מידע משוייך', 'framework'),
            'pages' => array('agent'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('כתובת דוא"ל', 'framework'),
                    'id' => "{$prefix}agent_email",
                    'desc' => __("Provide Agent Email Address. Agent related messages from contact form on property details page, will be sent on this email address.", "framework"),
                    'type' => 'text'
                ),
                array(
                    'name' => __('נייד', 'framework'),
                    'id' => "{$prefix}mobile_number",
                    'desc' => __("Provide Agent mobile number", "framework"),
                    'type' => 'text'
                ),
                array(
                    'name' => __('טלפון', 'framework'),
                    'id' => "{$prefix}office_number",
                    'desc' => __("Provide Agent office number", "framework"),
                    'type' => 'text'
                ),
                array(
                    'name' => __('פקס', 'framework'),
                    'id' => "{$prefix}fax_number",
                    'desc' => __("Provide Agent fax number", "framework"),
                    'type' => 'text'
                ),
                array(
                    'name' => __('Facebook URL', 'framework'),
                    'id' => "{$prefix}facebook_url",
                    'desc' => __("Provide Agent Facebook URL", "framework"),
                    'type' => 'text'
                ),
                array(
                    'name' => __('Twitter URL', 'framework'),
                    'id' => "{$prefix}twitter_url",
                    'desc' => __("Provide Agent Twitter URL", "framework"),
                    'type' => 'text'
                ),
                array(
                    'name' => __('Google Plus URL', 'framework'),
                    'id' => "{$prefix}google_plus_url",
                    'desc' => __("Provide Agent Google Plus URL", "framework"),
                    'type' => 'text'
                ),
                array(
                    'name' => __('LinkedIn URL', 'framework'),
                    'id' => "{$prefix}linked_in_url",
                    'desc' => __("Provide Agent LinkedIn URL", "framework"),
                    'type' => 'text'
                )
            )
        );


        // Banner Meta Box
        $meta_boxes[] = array(
            'id' => 'banner-meta-box',
            'title' => __('Tהגדרות איזור באנר עליון', 'framework'),
            'pages' => array('page', 'agent'),
            'context' => 'normal',
            'priority' => 'low',
            'fields' => array(
                array(
                    'name' => __('כותרת באנר', 'framework'),
                    'id' => "{$prefix}banner_title",
                    'desc' => __('Please provide the Banner Title, Otherwise the Page Title will be displayed in its place.', 'framework'),
                    'type' => 'text'
                ),
                array(
                    'name' => __('תת-כותרת לבאנר', 'framework'),
                    'id' => "{$prefix}banner_sub_title",
                    'desc' => __('Please provide the Banner Sub Title.', 'framework'),
                    'type' => 'textarea',
                    'cols' => '20',
                    'rows' => '2'
                ),
                array(
                    'name' => __('תמונת באנר', 'framework'),
                    'id' => "{$prefix}page_banner_image",
                    'desc' => __('Please upload the Banner Image. Otherwise the default banner image from theme options will be displayed.', 'framework'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1
                ),
                array(
                    'name' => __('Revolution Slider Alias', 'framework'),
                    'id' => "{$prefix}rev_slider_alias",
                    'desc' => __('If you want to replace banner with revolution slider then provide its alias here.', 'framework'),
                    'type' => 'text'
                )
            )
        );

        // Page title show or hide
        $meta_boxes[] = array(
            'id' => 'page-title-meta-box',
            'title' => __('כותרת דף', 'framework'),
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'low',
            'fields' => array(
                array(
                    'name' => __('כותרת דף מציגה סטטוס', 'framework'),
                    'id' => "{$prefix}page_title_display",
                    'type' => 'radio',
                    'std' => 'show',
                    'options' => array(
                        'show' => __('Show', 'framework'),
                        'hide' => __('Hide', 'framework')
                    )
                )
            )
        );

        /*
         * Meta boxes for properties list pages
         */
        $locations = array();
        inspiry_get_terms_array( 'property-city', $locations );

        $types = array();
        inspiry_get_terms_array( 'property-type', $types );

        $statuses = array();
        inspiry_get_terms_array( 'property-status', $statuses );

        $features = array();
        inspiry_get_terms_array( 'property-feature', $features );

        $meta_boxes[] = array(
            'id'        => 'properties-list-meta-box',
            'title'     => __( 'סנן הגדרות פרויקט', 'framework' ),
            'pages'     => array( 'page' ),
            'context'   => 'normal',
            'priority'  => 'high',
            'show'   => array(
                'template'    => array(
                    'template-property-listing.php',
                    'template-property-grid-listing.php',
                    'template-map-based-listing.php',
                ),
            ),
            'fields' => array(
                array(
                    'id'    => 'inspiry_posts_per_page',
                    'name'  => __( 'מספר פרויקטיםבעמוד', 'framework' ),
                    'type'  => 'number',
                    'step'  => '1',
                    'min'   => 1,
                    'std'   => 6,
                ),
                array(
                    'id'          => "inspiry_properties_order",
                    'name'        => __( 'סדר פרויקטיםלפי', 'framework' ),
                    'type'        => 'select',
                    'options'     => array(
                        'date-desc'     => __( 'חדש לישן', 'framework' ),
                        'date-asc'      => __( 'ישן לחדש', 'framework' ),
                        'price-asc'     => __( 'מחיר נמוך לגבוה', 'framework' ),
                        'price-desc'    => __( 'מחיר גבוה לנמוך', 'framework' ),
                    ),
                    'multiple'    => false,
                    'std'         => 'date-desc',
                ),
                array(
                    'id'          => "inspiry_properties_locations",
                    'name'        => __( 'מיקום', 'framework' ),
                    'type'        => 'select',
                    'options'     => $locations,
                    'multiple'    => true,
                ),
                array(
                    'id'          => "inspiry_properties_statuses",
                    'name'        => __( 'סטטוסים', 'framework' ),
                    'type'        => 'select',
                    'options'     => $statuses,
                    'multiple'    => true,
                ),
                array(
                    'id'          => "inspiry_properties_types",
                    'name'        => __( 'סוגים', 'framework' ),
                    'type'        => 'select',
                    'options'     => $types,
                    'multiple'    => true,
                ),
                array(
                    'id'          => "inspiry_properties_features",
                    'name'        => __( 'מאפיינים', 'framework' ),
                    'type'        => 'select',
                    'options'     => $features,
                    'multiple'    => true,
                ),
                array(
                    'id'    => 'inspiry_properties_min_beds',
                    'name'  => __( 'מינימום חדרי שינה', 'framework' ),
                    'type'  => 'number',
                    'step'  => 'any',
                    'min'   => 0,
                    'std'   => 0,
                ),
                array(
                    'id'    => 'inspiry_properties_min_baths',
                    'name'  => __( 'מינימום חדרי רחצה', 'framework' ),
                    'type'  => 'number',
                    'step'  => 'any',
                    'min'   => 0,
                    'std'   => 0,
                ),
                array(
                    'id'    => 'inspiry_properties_min_price',
                    'name'  => __( 'מחיר מינימאלי', 'framework' ),
                    'type'  => 'number',
                    'step'  => 'any',
                    'min'   => 0,
                    'std'   => 0,
                ),
                array(
                    'id'    => 'inspiry_properties_max_price',
                    'name'  => __( 'מחיר מקסימאלי', 'framework' ),
                    'type'  => 'number',
                    'step'  => 'any',
                    'min'   => 0,
                    'std'   => 0,
                ),
            )
        );

        // apply a filter before returning meta boxes
        $meta_boxes = apply_filters('framework_theme_meta', $meta_boxes);

        return $meta_boxes;

    }
}

?>