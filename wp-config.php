<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tabaplus_kcs');

/** MySQL database username */
define('DB_USER', 'tabaplus_kcs');

/** MySQL database password */
define('DB_PASSWORD', 'fdgh567y67g');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']v.--O3-;-l{6q7pbJ{N#K+w`V BJI;:KXgZ9lMN,>}0C)]tK5?y.mC4YL@-yz#@');
define('SECURE_AUTH_KEY',  '-2k~/b~ZXtP1Zu]Z+Mqe}{c{OJOw:-m,b,~!*kv^k55=HGC]a<%|pThx-8rR%^^a');
define('LOGGED_IN_KEY',    '$)q wJB#.CVT,:$#`5T6|:U#x|d>3jU%`@@*%XK/LVT^-W/1S^ee)QAC];m0+9Yz');
define('NONCE_KEY',        'Ir2-w!>`-F}I5lc~P-*6lK^ATzy|;x#!g6.o5Ew@qu063/~O88kde;?Z89Xzsy+O');
define('AUTH_SALT',        'f@5.Bg1zl/vhhi%$+WG!e/}1E }q$2H-Lzh--)R?+Bd5>KlSh<epeB;H:D~DF21y');
define('SECURE_AUTH_SALT', '8ox{L$+nT9B&62Y1!Ld#$k`7];aA25~AI f@fS)2C_t/{PevlB=Cl@>UrF56_vS|');
define('LOGGED_IN_SALT',   '5T2s; a8PXn r~;zv#WGw*%$rS`-J>K4KL:9],(R;I36bb|@# @zB0;|g*)Ov/h7');
define('NONCE_SALT',       'G}^S@!sG:}Tfw6/BWAT#_3hWDVLd`4d.u+P0GOl!xoY|(pRTm-b(C$2 <+A?uwig');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
